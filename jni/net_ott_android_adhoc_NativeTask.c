//#include <stdio.h>

//#include <stdlib.h>
//#include <string.h>
//#include <unistd.h>
//#include <cutils/properties.h>
//#include <sys/system_properties.h>

#include "net_ott_android_adhoc_NativeTask.h"

/*JNIEXPORT jstring JNICALL Java_net_ott_android_adhoc_NativeTask_getProp
  (JNIEnv *env, jclass class, jstring name)
{
  const char *nameString;
  nameString = (*env)->GetStringUTFChars(env, name, 0);

  //char value[PROPERTY_VALUE_MAX];
  char value[1024];
  char *default_value;
  jstring jstrOutput;
  
  default_value = "undefined";
  property_get(nameString, value, default_value);

  jstrOutput = (*env)->NewStringUTF(env, value);

  (*env)->ReleaseStringUTFChars(env, name, nameString);

  return jstrOutput;
}*/

JNIEXPORT jint JNICALL
Java_net_ott_android_adhoc_NativeTask_runCommand(
		JNIEnv *env, jclass class, jstring command)
{
	const char *commandString;
	int exitcode;

	commandString = (*env)->GetStringUTFChars(env, command, 0);
	exitcode = system(commandString);
	(*env)->ReleaseStringUTFChars(env, command, commandString);
	return (jint)exitcode;
}
