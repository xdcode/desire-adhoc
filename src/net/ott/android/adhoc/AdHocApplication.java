/**
 * desire-adhoc
 * Copyright (C) 2010-2011 Stefan Ott.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, see <http://www.gnu.org/licenses/>.
 * Use this application at your own risk.
 */

package net.ott.android.adhoc;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import net.ott.android.adhoc.network.NetworkState;
import net.ott.android.adhoc.network.UndefinedWifi;
import android.app.Application;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

public class AdHocApplication extends Application
{	
	public static final String TAG = "adhoc-app";
	public SharedPreferences settings = null;
	private WifiManager wifiManager = null;
	private NetworkState networkState;

	static void debug(String source, String message)
	{
		Log.d("AdHoc : " + source, message);
	}

	static void error(String source, String message)
	{
		Log.e("AdHoc : " + source, message);
	}

	public void debug(String message)
	{
		AdHocApplication.debug("Application", message);
	}

	private void error(String message)
	{
		AdHocApplication.error("Application", message);
	}

	@Override
	public void onCreate()
	{
		this.debug("Initializing");
		this.settings = PreferenceManager.getDefaultSharedPreferences(this);
		this.wifiManager = (WifiManager) this.getSystemService(WIFI_SERVICE);
		this.networkState = new UndefinedWifi(this);
		this.syncState();

		try {
			this.copyFromAsset("iwconfig");
			this.setFileExecutable("iwconfig");
		} catch (IOException e) {
			this.error("Exception: " + e.getMessage());
			this.longToast("Error extracting iwconfig");
		}
		try {
			this.copyFromAsset("dnsmasq");
			this.setFileExecutable("dnsmasq");
		} catch (IOException e) {
			this.error("Exception: " + e.getMessage());
			this.longToast("Error extracting dnsmasq");
		}
		/*
		try {
			this.copyFromAsset("dnsmasq.conf");
			this.copyFromAsset("resolv.conf");
		} catch (IOException e) {
			this.error("Exception: " + e.getMessage());
			this.longToast("Error extracting conf");
		}
		*/
		
		String basePath = this.getFilesDir().getParent();
		Log.d("hola", "basepath:" + basePath);
		File dir = new File(basePath + "/var");
		if (dir.exists() == false) {
			if (!dir.mkdir()) {
				Log.d("hola", "No se pudo crear el directorio /var!");
			}
		} else {
			Log.d("hola", "El directorio '" + dir.getAbsolutePath()
					+ "' ya existe!");
			if (dir.isDirectory()) { //delete its content
				File[] listFiles = dir.listFiles();
				for (int i = 0; i < listFiles.length; i++) {
					listFiles[i].delete();
					Log.d("hola", "Borrando " + listFiles[i].getName() + "...");
					try {
						Log.d("cat result", "cat " + listFiles[i].getName() + " :" + cat(listFiles[i].getName()));
					} catch (IOException e) {
						Log.e("cat error", e.getMessage());
					} catch (InterruptedException e) {
						Log.e("cat error", e.getMessage());
					}
					Log.d("hola", "Borrando " + listFiles[i].getName() + "...");
				}
			}
		}

	}
	
	public void go()
	{
		this.networkState = this.networkState.go();
	}

	public void startAdHoc()
	{
		this.debug("Starting ad-hoc network");

		String ssid = this.settings.getString("ssid", "any");
		String bssid = this.settings.getString("bssid", "");
		String channel = this.settings.getString("channel", "0");
		boolean ap = this.settings.getBoolean("ap", false);
		String ip = this.settings.getString("ip", "192.168.1.55");
		String netmask = this.settings.getString("netmask", "255.255.255.0");
		String gateway = this.settings.getString("gateway", "192.168.1.1");
		String nameserver = this.settings.getString("nameserver", "8.8.8.8");
		boolean dhcp = this.settings.getBoolean("dhcp", false);
		boolean dnsmasq = this.settings.getBoolean("dnsmasq", false);
		String ip6 = this.settings.getString("ip6", "fc00:6680::1");
		String mask6 = this.settings.getString("netmask6", "64");
		boolean manualip6 = this.settings.getBoolean("ip6manual", false);

		if (channel.equals("0"))
		{
			channel = "any";
		}

		if (bssid.equals(""))
		{
			bssid = "any";
		}

		this.debug("ssid    = " + ssid);
		this.debug("bssid   = " + bssid);
		this.debug("channel = " + channel);
		this.debug("dhcp    = " + dhcp);
		this.debug("ip      = " + ip);
		this.debug("mask    = " + netmask);
		this.debug("gw      = " + gateway);
		this.debug("dns     = " + nameserver);
		this.debug("ip6     = " + ip6);
		this.debug("mask6   = " + mask6);

		String basePath = this.getFilesDir().getParent();
		if (ap) {
			//mode master (access point)
			this.runRootCommand("insmod /system/lib/modules/bcm4329.ko " +
					"\"firmware_path=/system/etc/firmware/fw_bcm4329_apsta.bin\"");
			this.runRootCommand(basePath + "/files/iwconfig eth0 mode master");
		} else {
			//ad-hoc network
			this.runRootCommand("insmod /system/lib/modules/bcm4329.ko");
			this.runRootCommand(basePath + "/files/iwconfig eth0 mode ad-hoc");
		}
		this.runRootCommand(basePath + "/files/iwconfig eth0 channel " + channel);
		this.runRootCommand(basePath + "/files/iwconfig eth0 essid " + ssid);
		
		this.runRootCommand(basePath + "/files/iwconfig eth0 commit");


		// we do this twice because a single run doesn't always work 
//		this.runRootCommand(basePath + "/files/iwconfig eth0 ap " + bssid);
//		this.runRootCommand(basePath + "/files/iwconfig eth0 ap " + bssid);
		
		//this.runRootCommand(basePath + "/files/iwconfig eth0 essid " + ssid);
		
		if (dhcp) {
			this.runRootCommand("netcfg eth0 dhcp");
		} else {
			this.runRootCommand("ifconfig eth0 " + ip + " netmask " + netmask);
			this.runRootCommand("ifconfig eth0 up");
			if (gateway != "") {
				this.runRootCommand("ip route add default via " + gateway);
//				this.runRootCommand("ip rule add to " + ip +"/24 lookup main priority 50");
//			    this.runRootCommand("ip route flush cache");
			}
			
			if (dnsmasq) {
				//kill dnsmasq
				killProcess(basePath + "/files/dnsmasq");
				//instantiate new one
				this.runRootCommand(basePath + "/files/dnsmasq -i eth0 " +
						" --dhcp-authoritative --no-negcache --user=root "+
						"--no-resolv --no-hosts "+
						"--server="+ "8.8.8.8" +" "+
						"--server="+ "4.4.4.4" +" "+
						"--dhcp-range="+ "192.168.1.101,192.168.1.105,255.255.255.0,1h" +" " +
						"--dhcp-leasefile=/data/data/net.ott.android.adhoc/var/dnsmasq.leases " +
						"--pid-file=/data/data/net.ott.android.adhoc/var/dnsmasq.pid");
			/*
            this.runRootCommand(basePath + "/files/dnsmasq.bin -i eth0 " +
					"--resolv-file=" + basePath + "/files/resolv.conf" + 
					" --conf-file=" + basePath + "/files/dnsmasq.conf");
			*/	
			} else {
				this.runRootCommand("setprop net.dns1 " + nameserver);
			}
		}

		if (manualip6)
		{
			this.runRootCommand("ip -6 addr add " + ip6 + "/" + mask6 + " dev eth0");
		}
	}

	public void stopAdHoc()
	{
		this.debug("Stopping ad-hoc network");
		String basePath = this.getFilesDir().getParent();
		killProcess(basePath + "/files/dnsmasq");
		this.runRootCommand("svc wifi disable");
	}

	public boolean androidWifiOn()
	{
		boolean enabled = this.wifiManager.isWifiEnabled();
		this.debug("Android wifi enabled? " + enabled);
		return enabled;
	}
	
	public boolean wlanDevExists()
	{
		File proc = new File("/proc/net/wireless");
		boolean found = false;

		try {
			LineNumberReader fin = new LineNumberReader(new FileReader(proc));
			String line;

			while ((line = fin.readLine()) != null) {
				if (line.contains("eth0"))
					found = true;
			}
			fin.close();
		} catch (FileNotFoundException e) {
			return found;
		} catch (IOException e) {
			return found;
		}

		return found;
	}

	public void disableWifi()
	{
		this.debug("Disabling wifi");
		this.wifiManager.setWifiEnabled(false);
	}

	public void enableWifi()
	{
		this.debug("Enabling wifi");
		this.wifiManager.setWifiEnabled(true);
	}

	public void longToast(String message)
	{
		this.toast(message, Toast.LENGTH_LONG);
	}

	public void shortToast(String message)
	{
		this.toast(message, Toast.LENGTH_SHORT);
	}

	private void toast(String message, int length)
	{
		Toast toast = Toast.makeText(getApplicationContext(), message, length);
		toast.show();
	}

	private void copyFromAsset(String fileName) throws IOException
	{

		String basePath = this.getFilesDir().getParent();
		File dest = new File(basePath + "/" + fileName);
		
		this.debug("Copying " + fileName + " from assets, in " + basePath);
		
		if (dest.exists())
		{
			this.debug("File " + fileName + " exists, not copying");
			return;
		}

		InputStream in = this.getAssets().open(fileName);
		OutputStream out = openFileOutput(fileName, MODE_PRIVATE);

		byte[] buffer = new byte[10240];
		int length;

		while ((length = in.read(buffer)) > 0)
		{
			out.write(buffer, 0, length);
		}

		in.close();
		out.close();
	}

	private void setFileExecutable(String fileName) throws IOException
	{
		this.debug("Setting " + fileName + " executable ");

		String basePath = this.getFilesDir().getPath();
		Runtime.getRuntime().exec("chmod 755 " + basePath + "/" + fileName);
	}

	private boolean runRootCommand(String command)
	{
		return this.runCommand("su -c \"" + command + "\"");
	}

	private boolean runCommand(String command)
	{
		this.debug("Command: " + command);

		int result = NativeTask.runCommand(command);

		if (result == 0)
			return true;

		this.debug("Command error, return code: " + result);
		return false;
	}

	public int getStatusText()
	{
		return this.networkState.getStatusText();
	}

	public int getButtonText()
	{
		return this.networkState.getButtonText();
	}

	public void syncState()
	{
		this.networkState = this.networkState.sync();
	}
	
    public static String cat(String file) throws IOException, InterruptedException {
        StringBuffer echo = new StringBuffer();
        Runtime runtime = Runtime.getRuntime();
        Log.v("cat", "cat using runtime.exec");
        Process proc = runtime.exec("cat " + file);
        proc.waitFor();
        int exit = proc.exitValue();
        if (exit == 0) {
            InputStreamReader reader = new InputStreamReader(proc.getInputStream());
            BufferedReader buffer = new BufferedReader(reader);
            String line = "";
            while ((line = buffer.readLine()) != null) {
                echo.append(line + "\n");
            }           
            return echo.toString();
        }
        return "fail";
    }
    
    /** M�todo para matar un proceso dado su nombre */
    public void killProcess(String processName) {
    	int pidToKill;
    	int start = 0, leftIndex, end;
    	String pid = " ";
    	List<String> psOutput = runSuperuserCommand("ps");
    	for (String proc : psOutput) {
    		if (proc.contains(processName)) {
    			leftIndex = proc.indexOf(" ");
    			for (int i = leftIndex; i < proc.length(); i++) {
    				char c = proc.charAt(i);
    				if (!Character.isWhitespace(c)) {
    					start = i;
    					break;
    				}
    			}
    			end = proc.indexOf(" ", start);
    			if (start != 0 && end != 0 && end >= start) {
        			pid = proc.substring(start, end);
    			}
    			pidToKill = Integer.parseInt(pid);
    			Log.d("kill", "killProcess: " + processName + ", pid: '" 
    					+ pidToKill + "'" );
    			runSuperuserCommand("kill " + pidToKill);
    		}
    	}
    }
    public List<String> runSuperuserCommand (String command) {
    	Runtime rt = Runtime.getRuntime();
    	Process proc = null;
    	OutputStreamWriter osw = null;
    	DataInputStream err = null;
    	DataInputStream out = null;
    	List<String> outputStream = null;
    	List<String> errorStream = null;
    	outputStream = new ArrayList<String>();
    	errorStream = new ArrayList<String>();
    	try {
    		proc = rt.exec("su");
    		osw = new OutputStreamWriter(proc.getOutputStream());
    		err = new DataInputStream(proc.getErrorStream());
    		out = new DataInputStream(proc.getInputStream());
    		Log.d(TAG, "Ejecutando: " + command);
    		osw.write(command);
    		osw.flush();
    		osw.close();
    	} catch (Exception e) {
    		Log.e(TAG, "Error runSuperuserCommand: " + command + " / " + e);
    		
    	} finally {
    		try {
    			errorStream.clear();
    			String error;
                while ((error = err.readLine()) != null) {
                	errorStream.add(error);
                	Log.d(TAG, "Error stream: " + error);
                }
                outputStream.clear();
                String output;
                while((output = out.readLine()) != null) {
                	outputStream.add(output);
//                	Log.d(TAG, "Output stream: " + output);
                }
    		} catch (Exception e) {
    			Log.e(TAG, "Error streams: " + e);
    		}
            if (osw != null) {
                try {
                    osw.close();
                } catch (Exception e){}
            }
        }

        try {
            proc.waitFor();
        } catch (InterruptedException e){}

        if (proc.exitValue() != 0) {
            Log.e(TAG, "Error runSuperuserCommand: " + command + 
            		" / exit value: " + proc.exitValue());
        }
        return outputStream;
    }

    
 /*   
    public Hashtable<String,ClientData> getLeases() throws Exception {
        Hashtable<String,ClientData> returnHash = new Hashtable<String,ClientData>();
        
        ClientData clientData;
        
        ArrayList<String> lines = readLinesFromFile(+"/var/dnsmasq.leases");
        
        for (String line : lines) {
                        clientData = new ClientData();
                        String[] data = line.split(" ");
                        Date connectTime = new Date(Long.parseLong(data[0] + "000"));
                        String macAddress = data[1];
                        String ipAddress = data[2];
                        String clientName = data[3];
                        clientData.setConnectTime(connectTime);
                        clientData.setClientName(clientName);
                        clientData.setIpAddress(ipAddress);
                        clientData.setMacAddress(macAddress);
                        clientData.setConnected(true);
                        returnHash.put(macAddress, clientData);
                }
        return returnHash;
    }
   */ 
}