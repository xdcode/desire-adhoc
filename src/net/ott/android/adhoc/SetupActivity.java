/**
 * desire-adhoc
 * Copyright (C) 2010-2011 Stefan Ott.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, see <http://www.gnu.org/licenses/>.
 * Use this application at your own risk.
 */

package net.ott.android.adhoc;

import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.Preference.OnPreferenceChangeListener;

public class SetupActivity extends PreferenceActivity implements OnSharedPreferenceChangeListener
{
	private AdHocApplication application = null;

	private void debug(String message)
	{
		AdHocApplication.debug("SetupActivity", message);
	}

	@Override
	public void onCreate(Bundle state)
	{
		super.onCreate(state);

		this.application = (AdHocApplication) this.getApplication();

		class IPAddressFormatChangeListener implements OnPreferenceChangeListener
		{
			private EditTextPreference parent;

			public IPAddressFormatChangeListener(EditTextPreference parent)
			{
				this.parent = parent;
			}

			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue)
			{
				String message = newValue.toString();

				try {
					InetAddress.getByName(message);
				} catch (UnknownHostException e) {
					SetupActivity.this.application.longToast(getString(R.string.pref_ip_invalid));
					return false;
				}

				this.parent.setSummary(message);
				return true;
			}
		}
		class IP6AddressFormatChangeListener implements OnPreferenceChangeListener
		{
			private EditTextPreference parent;

			public IP6AddressFormatChangeListener(EditTextPreference parent)
			{
				this.parent = parent;
			}
			
			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue)
			{
				String message = newValue.toString();
				
				try {
					Inet6Address.getByName(message);
				} catch (UnknownHostException e) {
					SetupActivity.this.application.longToast(getString(R.string.pref_ip_invalid));
					return false;
				}
				
				this.parent.setSummary(message);
				return true;
			}
		}
		class Mask6FormatChangeListener implements OnPreferenceChangeListener
		{
			private EditTextPreference parent;

			public Mask6FormatChangeListener(EditTextPreference parent)
			{
				this.parent = parent;
			}

			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue)
			{
				String message = newValue.toString();
				Integer mask;
				
				try {
					mask = Integer.parseInt(message);
				} catch (Exception e) {
					SetupActivity.this.application.longToast(getString(R.string.pref_netmask_invalid));
					return false;
				}
				
				if ((mask >= 0) && (mask <= 128))
				{
					this.parent.setSummary(message);
					return true;
				}

				SetupActivity.this.application.longToast(getString(R.string.pref_netmask_invalid));
				return false;
			}
		}
		class SSIDFormatChangeListener implements OnPreferenceChangeListener
		{
			private EditTextPreference parent;

			public SSIDFormatChangeListener(EditTextPreference parent)
			{
				this.parent = parent;
			}

			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue)
			{
				String message = newValue.toString();

				if (message.matches("^[\\.:_a-zA-Z0-9]+$"))
				{
					this.parent.setSummary(message);
					return true;
				}
				SetupActivity.this.application.longToast(getString(R.string.pref_ip_invalid));
				return false;
			}
		}
		class BSSIDFormatChangeListener implements OnPreferenceChangeListener
		{
			private EditTextPreference parent;

			public BSSIDFormatChangeListener(EditTextPreference parent)
			{
				this.parent = parent;
			}

			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue)
			{
				String message = newValue.toString();

				if (message.matches("^[\\.:_a-zA-Z0-9]+$"))
				{
					this.parent.setSummary(message);
					return true;
				}
				else if (message.equals(""))
				{
					this.parent.setSummary(SetupActivity.this.getString(R.string.any));
					return true;
				}
				SetupActivity.this.application.longToast(getString(R.string.pref_ip_invalid));
				return false;
			}
		}
		class ChannelFormatChangeListener implements OnPreferenceChangeListener
		{
			private ListPreference parent;
			
			public ChannelFormatChangeListener(ListPreference parent)
			{
				this.parent = parent;
			}
			
			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue)
			{
				String channel = newValue.toString();

				if (channel.matches("^[0-9]+$"))
				{
					CharSequence[] values = this.parent.getEntryValues();
					CharSequence[] entries = this.parent.getEntries();
					
					for (int i=0; i < entries.length; i++)
					{
						if (values[i] == channel)
						{
							this.parent.setSummary(entries[i]);
							return true;
						}
					}
					SetupActivity.this.application.longToast(getString(R.string.pref_chan_not_found));
					return false;
				}
				SetupActivity.this.application.longToast(getString(R.string.pref_chan_invalid));
				return false;
			}
		}
		

		this.addPreferencesFromResource(R.layout.setup);

		CheckBoxPreference dhcp = (CheckBoxPreference)this.findPreference("dhcp");
		dhcp.setDisableDependentsState(true);

		// ssid
		EditTextPreference ssid = (EditTextPreference)this.findPreference("ssid");
		ssid.setSummary(ssid.getText());
		ssid.setOnPreferenceChangeListener(new SSIDFormatChangeListener(ssid));

		// bssid
		EditTextPreference bssid = (EditTextPreference)this.findPreference("bssid");
		String text = bssid.getText();
		bssid.setSummary(text.equals("") ? this.getString(R.string.any) : text);
		bssid.setOnPreferenceChangeListener(new BSSIDFormatChangeListener(bssid));

		//channel
		ListPreference channel = (ListPreference)this.findPreference("channel");
		channel.setSummary(channel.getEntry());
		channel.setOnPreferenceChangeListener(new ChannelFormatChangeListener(channel));

		// ip address
		EditTextPreference ip = (EditTextPreference)this.findPreference("ip");
		ip.setSummary(ip.getText());
		ip.setOnPreferenceChangeListener(new IPAddressFormatChangeListener(ip));

		// netmask
		EditTextPreference netmask = (EditTextPreference)this.findPreference("netmask");
		netmask.setSummary(netmask.getText());
		netmask.setOnPreferenceChangeListener(new IPAddressFormatChangeListener(netmask));

		// gateway
		final EditTextPreference gateway = (EditTextPreference)this.findPreference("gateway");
		gateway.setSummary(gateway.getText());
		gateway.setOnPreferenceChangeListener(new IPAddressFormatChangeListener(gateway));
		
		// name server
		final EditTextPreference nameserver = (EditTextPreference)this.findPreference("nameserver");
		nameserver.setSummary(nameserver.getText());
		nameserver.setOnPreferenceChangeListener(new IP6AddressFormatChangeListener(nameserver));
		
		//dnsmasq
		final CheckBoxPreference dnsmasq = (CheckBoxPreference)this.findPreference("dnsmasq");
		dnsmasq.setOnPreferenceChangeListener(new OnPreferenceChangeListener(){
	            @Override
	            public boolean onPreferenceChange(Preference preference, Object newValue) {
	                if (preference == dnsmasq && newValue.toString().equals("true")){
	                	nameserver.setEnabled(false);
	                	gateway.setEnabled(false);
	                }
	                if (preference == dnsmasq && newValue.toString().equals("false")){
	                	nameserver.setEnabled(true);
	                	gateway.setEnabled(true);
	                }
	                return true;    
	            }
		});
		
		// ipv6 address
		EditTextPreference ip6 = (EditTextPreference)this.findPreference("ip6");
		ip6.setSummary(ip6.getText());
		ip6.setOnPreferenceChangeListener(new IP6AddressFormatChangeListener(ip6));
		
		// ipv6 netmask
		EditTextPreference mask6 = (EditTextPreference)this.findPreference("netmask6");
		mask6.setSummary(mask6.getText());
		mask6.setOnPreferenceChangeListener(new Mask6FormatChangeListener(mask6));
		
	}

	@Override
	protected void onResume()
	{
		this.debug("Calling onResume()");
		super.onResume();
		getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	protected void onPause()
	{
		this.debug("Calling onPause()");
		super.onPause();
		getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);   
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key)
	{
		this.debug("Preference " + key + " changed");
	}
}