/**
 * desire-adhoc
 * Copyright (C) 2010-2011 Stefan Ott.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, see <http://www.gnu.org/licenses/>.
 * Use this application at your own risk.
 */

package net.ott.android.adhoc;

public class NativeTask
{
	static
	{
		try
		{
			AdHocApplication.debug("NativeTask", "Loading libNativeTask.so");
			System.load("/data/data/net.ott.android.adhoc/lib/libNativeTask.so");
		}
		catch (UnsatisfiedLinkError ule)
		{
			AdHocApplication.error("NativeTask", "Could not load libNativeTask.so"); 
		}
	}

	public static native int runCommand(String command);
}