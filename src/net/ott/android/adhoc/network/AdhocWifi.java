/**
 * desire-adhoc
 * Copyright (C) 2010-2011 Stefan Ott.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, see <http://www.gnu.org/licenses/>.
 * Use this application at your own risk.
 */

package net.ott.android.adhoc.network;

import net.ott.android.adhoc.AdHocApplication;
import net.ott.android.adhoc.R;

public class AdhocWifi extends NetworkState
{
	public AdhocWifi(AdHocApplication application)
	{
		super(application);
	}

	@Override
	public NetworkState go()
	{
		this.application.debug("Stopping adhoc networking");
		this.application.enableWifi();
		this.application.stopAdHoc();
		return new NoWifi(this.application);
	}

	@Override
	public int getButtonText()
	{
		return R.string.button_stop_adhoc;
	}

	@Override
	public int getStatusText()
	{
		return R.string.wifistatusadhoc;
	}	
}