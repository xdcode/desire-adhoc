/**
 * desire-adhoc
 * Copyright (C) 2010-2011 Stefan Ott.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, see <http://www.gnu.org/licenses/>.
 * Use this application at your own risk.
 */

package net.ott.android.adhoc.network;

import net.ott.android.adhoc.AdHocApplication;
import net.ott.android.adhoc.R;

public class NoWifi extends NetworkState
{
	public NoWifi(AdHocApplication application)
	{
		super(application);
	}

	@Override
	public NetworkState go()
	{
		this.application.debug("Starting adhoc networking");
		this.application.startAdHoc();
		return new AndroidWifi(this.application);
	}

	@Override
	public int getButtonText()
	{
		return R.string.button_start_adhoc;
	}

	@Override
	public int getStatusText()
	{
		return R.string.wifistatusoff;
	}
}