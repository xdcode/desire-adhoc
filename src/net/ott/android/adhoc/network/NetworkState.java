/**
 * desire-adhoc
 * Copyright (C) 2010-2011 Stefan Ott.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, see <http://www.gnu.org/licenses/>.
 * Use this application at your own risk.
 */

package net.ott.android.adhoc.network;

import net.ott.android.adhoc.AdHocApplication;

public abstract class NetworkState
{
	protected AdHocApplication application;
	
	protected NetworkState(AdHocApplication application)
	{
		this.application = application;
	}
	
	public NetworkState sync()
	{
		if (this.application.androidWifiOn())
			return new AndroidWifi(this.application);
		else if (this.application.wlanDevExists())
			return new AdhocWifi(this.application);
		else
			return new NoWifi(this.application);
	}
	
	public abstract NetworkState go();
	public abstract int getStatusText();
	public abstract int getButtonText();
}