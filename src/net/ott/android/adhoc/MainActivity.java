/**
 * desire-adhoc
 * Copyright (C) 2010-2011 Stefan Ott.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, see <http://www.gnu.org/licenses/>.
 * Use this application at your own risk.
 */

package net.ott.android.adhoc;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity
{
	private static final int MENU_SETUP = 0;
	private static final int MENU_REFRESH = 1;

	private AdHocApplication application = null;
	private Button startButton = null;
	private TextView statusText = null;
	private BroadcastReceiver receiver;

	final private Handler handler = new Handler();

	final Runnable updateScreen = new Runnable()
	{
		public void run()
		{
			updateScreen();
		}
	};

	private void debug(String message)
	{ 
		AdHocApplication.debug("MainActivity", message);
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		this.startButton = (Button) findViewById(R.id.button);
		this.statusText = (TextView) findViewById(R.id.status);
		this.application = (AdHocApplication)this.getApplication();

		// Start button
		this.startButton.setOnClickListener(new OnClickListener()
		{
			public void onClick(View v)
			{
				MainActivity.this.debug("Start button pressed");
				MainActivity.this.application.longToast(getString(R.string.pleasewait));
				new Thread(new Runnable()
				{
					public void run()
					{
						MainActivity.this.application.go();
						MainActivity.this.handler.post(MainActivity.this.updateScreen);
					}
				}).start();
			}
		});

		this.receiver = new BroadcastReceiver()
		{
			@Override
			public void onReceive(Context context, Intent intent)
			{
				MainActivity.this.debug("Wifi status changed");
				MainActivity.this.handler.post(MainActivity.this.updateScreen);
			}
		};
	}

	private void updateScreen()
	{
		this.application.syncState();
		this.statusText.setText(this.application.getStatusText());
		this.startButton.setText(this.application.getButtonText());
	}

	@Override
	protected void onPause()
	{
		this.debug("Calling onPause()");
		super.onPause();
		this.unregisterReceiver(this.receiver);
	}

	@Override
	protected void onResume()
	{
		this.debug("Calling onResume()");
		super.onResume();
		this.registerReceiver(this.receiver, new IntentFilter(WifiManager.WIFI_STATE_CHANGED_ACTION));
		this.updateScreen();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		boolean result = super.onCreateOptionsMenu(menu);
		MenuItem item;

		item = menu.add(0, MENU_REFRESH, 0, getString(R.string.refresh));
		item.setIcon(android.R.drawable.ic_menu_rotate);

		item = menu.add(0, MENU_SETUP, 0, getString(R.string.setuptext));
		item.setIcon(android.R.drawable.ic_menu_preferences);

		return result;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		this.debug("Menu item selected: " + item.getItemId());
		switch (item.getItemId())
		{
			case MENU_SETUP:
				startActivityForResult(new Intent(MainActivity.this, SetupActivity.class), 0);
				break;
			case MENU_REFRESH:
				this.application.shortToast(getString(R.string.refreshing));
				MainActivity.this.handler.post(MainActivity.this.updateScreen);
				break;
		}
		return false;
	}
}